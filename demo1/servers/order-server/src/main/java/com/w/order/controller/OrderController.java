package com.w.order.controller;


import com.w.base.Result;
import com.w.dto.OrderDto;
import com.w.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author w²
 * @since 2020-09-27
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;


    /**
     * 创建订单
     *
     * @param orderDto 订单信息
     * @return 创建结果
     */
    @PostMapping(value = "createOrder.html")
    @Transactional
    public Result createOrder(OrderDto orderDto) {
        return orderService.createOrder(orderDto);
    }

}
