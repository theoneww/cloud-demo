package com.w.order.mapper;

import com.w.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author w²
 * @since 2020-09-27
 */
public interface OrderMapper extends BaseMapper<OrderEntity> {

}
