package com.w.order.controller;

import com.w.base.Result;
import com.w.order.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName TestController
 * @Description 测试接口类
 * @Author w²
 * @Date 2020/9/27 22:40
 **/
@RestController
public class TestController {

    @Autowired
    private OrderMapper orderMapper;


    /**
     * 查询订单详情
     *
     * @param orderId 订单id
     * @return 订单详情
     */
    @GetMapping(value = "queryOrder")
    public Result queryOrder(Long orderId) {
        return Result.success(orderMapper.selectById(orderId));
    }

}
