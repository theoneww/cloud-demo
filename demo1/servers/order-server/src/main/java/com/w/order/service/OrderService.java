package com.w.order.service;

import com.w.base.Result;
import com.w.dto.OrderDto;
import com.w.order.entity.OrderEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author w²
 * @since 2020-09-27
 */
public interface OrderService extends IService<OrderEntity> {

    /**
     * 创建订单
     *
     * @param orderDto 订单信息
     * @return 创建结果
     */
    Result createOrder(OrderDto orderDto);

}
