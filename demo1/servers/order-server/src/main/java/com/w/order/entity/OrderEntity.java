package com.w.order.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.w.base.table.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author w²
 * @since 2020-09-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("`order`")
public class OrderEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 产品id
     */
    private Integer goodsId;

    /**
     * 产品数量
     */
    private Integer goodsCount;

    /**
     * 订单价格
     */
    private BigDecimal orderPrice;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;


}
