package com.w.goods.service;

import com.w.goods.entity.GoodsEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author w²
 * @since 2020-09-28
 */
public interface GoodsService extends IService<GoodsEntity> {

    /**
     * 扣减库存
     *
     * @param goodsId    商品id
     * @param goodsCount 商品数量
     */
    void deductStock(Integer goodsId, Integer goodsCount);
}
