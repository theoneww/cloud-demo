package com.w.goods.mapper;

import com.w.goods.entity.GoodsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author w²
 * @since 2020-09-28
 */
public interface GoodsMapper extends BaseMapper<GoodsEntity> {

}
