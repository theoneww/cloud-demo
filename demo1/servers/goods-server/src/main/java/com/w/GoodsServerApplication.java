package com.w;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @ClassName OrderServerApplication
 * @Description 订单服务启动类
 * @Author w²
 * @Date 2020/9/24 0024 15:52
 **/
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients // 开启feign
@EnableHystrix
@MapperScan("com.w.goods.mapper")
public class GoodsServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GoodsServerApplication.class, args);
    }

}
