package com.w.goods.controller;


import com.w.goods.entity.GoodsEntity;
import com.w.goods.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品表 前端控制器
 * </p>
 *
 * @author w²
 * @since 2020-09-28
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;


    /**
     * 查询商品信息
     *
     * @param goodsId 商品id
     * @return 商品信息
     */
    @GetMapping(value = "selectGoods")
    public GoodsEntity selectGoods(@RequestParam(value = "goodsId") Integer goodsId) {
        return goodsService.getById(goodsId);
    }

    /**
     * 扣减库存
     *
     * @param goodsId    商品id
     * @param goodsCount 商品数量
     */
    @GetMapping(value = "deductStock")
    public void deductStock(Integer goodsId, Integer goodsCount) {
        // 查询商品信息
        goodsService.deductStock(goodsId, goodsCount);
    }

}
