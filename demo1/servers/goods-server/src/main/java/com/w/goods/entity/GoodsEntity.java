package com.w.goods.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.w.base.table.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author w²
 * @since 2020-09-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("goods")
public class GoodsEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 单价
     */
    private BigDecimal price;

    /**
     * 商品数量
     */
    private Integer count;


}
