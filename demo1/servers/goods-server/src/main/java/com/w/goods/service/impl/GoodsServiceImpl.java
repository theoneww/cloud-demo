package com.w.goods.service.impl;

import com.w.goods.entity.GoodsEntity;
import com.w.goods.mapper.GoodsMapper;
import com.w.goods.service.GoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author w²
 * @since 2020-09-28
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, GoodsEntity> implements GoodsService {

    @Autowired
    private GoodsMapper goodsMapper;


    /**
     * 扣减库存
     *
     * @param goodsId    商品id
     * @param goodsCount 商品数量
     */
    @Override
    public void deductStock(Integer goodsId, Integer goodsCount) {
        // 查询商品信息
        GoodsEntity goodsEntity = goodsMapper.selectById(goodsId);
        if (goodsEntity == null) {
            throw new RuntimeException("商品信息非法");
        }
        // 判断商品数量
        if (goodsEntity.getCount() < goodsCount) {
            throw new RuntimeException("商品库存不足");
        }
        // 修改库存数量
        goodsEntity.setCount(goodsEntity.getCount() - goodsCount);
        goodsMapper.updateById(goodsEntity);
    }

}
