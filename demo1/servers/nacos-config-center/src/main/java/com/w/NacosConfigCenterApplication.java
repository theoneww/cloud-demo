package com.w;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @ClassName NacosConfigCenterApplication
 * @Description nacos配置中心启动类
 * @Author w²
 * @Date 2020/9/27 0027 11:17
 **/
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class}) // 用于排除 jdbc 的自动装配机制
public class NacosConfigCenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosConfigCenterApplication.class, args);
    }

}
