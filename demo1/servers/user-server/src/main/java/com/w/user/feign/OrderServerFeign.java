package com.w.user.feign;

import com.w.base.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * orderServer feign，用于调用order服务
 */
@Component
@FeignClient(name = "order-server")
public interface OrderServerFeign {

    /**
     * 创建订单
     *
     * @param orderId 订单id
     * @return 创建结果
     */
    @GetMapping(value = "queryOrder")
    Result queryOrder(@RequestParam(value = "orderId") Long orderId);

}
