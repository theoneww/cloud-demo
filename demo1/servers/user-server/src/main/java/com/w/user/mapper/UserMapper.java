package com.w.user.mapper;

import com.w.user.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author w²
 * @since 2020-09-24
 */
public interface UserMapper extends BaseMapper<UserEntity> {

}
