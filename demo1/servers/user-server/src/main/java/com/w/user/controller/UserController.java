package com.w.user.controller;


import com.w.user.entity.UserEntity;
import com.w.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;


/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author w²
 * @since 2020-09-24
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;


    /**
     * 获取用户信息
     *
     * @param userId 用户id
     * @return 用户信息
     */
    @GetMapping(value = "selectUser")
    public UserEntity user(Integer userId) {
        return userService.getById(userId);
    }

    /**
     * 修改用户余额
     *
     * @param userEntity 用户信息
     * @return 修改结果
     */
    @PostMapping(value = "updateUserBalance")
    public Boolean updateUserBalance(@RequestBody UserEntity userEntity) {
        return userService.updateById(userEntity);
    }

    /**
     * 扣减余额
     *
     * @param userId     用户id
     * @param orderPrice 订单价格
     */
    @GetMapping(value = "reduceBalance")
    public void reduceBalance(Integer userId, BigDecimal orderPrice) {
        userService.reduceBalance(userId, orderPrice);
    }

}
