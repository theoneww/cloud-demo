package com.w.user.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.w.base.table.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author w²
 * @since 2020-09-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user")
public class UserEntity extends BaseEntity {

    /**
     * 姓名
     */
    private String name;

    /**
     * 余额
     */
    private BigDecimal balance;


}
