package com.w.user.service;

import com.w.user.entity.UserEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author w²
 * @since 2020-09-24
 */
public interface UserService extends IService<UserEntity> {

    /**
     * 扣减余额
     *
     * @param userId     用户id
     * @param orderPrice 订单价格
     */
    void reduceBalance(Integer userId, BigDecimal orderPrice);
}
