package com.w.api;

import com.w.api.hystrix.OrderInnerApiFailure;
import com.w.dto.OrderDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @ClassName OrderInnerApi
 * @Description 订单内部api
 * @Author w²
 * @Date 2020/9/28 0028 14:19
 **/
@FeignClient(name = "order-server", path = "order", fallback = OrderInnerApiFailure.class)
public interface OrderInnerApi {

    /**
     * 创建订单
     *
     * @param orderDto 订单信息
     * @return 创建结果
     */
    @PostMapping(value = "insertOrder")
    Boolean insertOrder(@RequestBody OrderDto orderDto);

}
