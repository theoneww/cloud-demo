package com.w.dto;

import com.w.base.table.BaseDto;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @ClassName OrderDto
 * @Description 订单数据传输对象
 * @Author w²
 * @Date 2020/9/28 0028 14:32
 **/
@Data
public class OrderDto extends BaseDto {

    /**
     * 产品id
     */
    private Integer goodsId;

    /**
     * 产品数量
     */
    private Integer goodsCount;

    /**
     * 订单价格
     */
    private BigDecimal orderPrice;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

}
