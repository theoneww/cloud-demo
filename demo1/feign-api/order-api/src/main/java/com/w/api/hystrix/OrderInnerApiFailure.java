package com.w.api.hystrix;

import com.w.api.OrderInnerApi;
import com.w.dto.OrderDto;

/**
 * @ClassName OrderInnerApiFailure
 * @Description
 * @Author w²
 * @Date 2020/9/28 0028 17:38
 **/
public class OrderInnerApiFailure implements OrderInnerApi {
    /**
     * 创建订单
     *
     * @param orderDto 订单信息
     * @return 创建结果
     */
    @Override
    public Boolean insertOrder(OrderDto orderDto) {
        throw new RuntimeException("创建订单时熔断");
    }
}
