package com.w.vo;

import com.w.base.table.BaseVo;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @ClassName OrderVo
 * @Description 订单视图对象
 * @Author w²
 * @Date 2020/9/28 0028 15:46
 **/
@Data
public class OrderVo extends BaseVo {

    /**
     * 产品id
     */
    private Integer goodsId;

    /**
     * 产品数量
     */
    private Integer goodsCount;

    /**
     * 订单价格
     */
    private BigDecimal orderPrice;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 添加时间
     */
    private LocalDateTime addTime;

}
