package com.w.api.hystrix;

import com.w.api.GoodsInnerApi;
import com.w.vo.GoodsVo;

/**
 * @ClassName GoodsInnerApiFailure
 * @Description
 * @Author w²
 * @Date 2020/9/28 0028 17:35
 **/
public class GoodsInnerApiFailure implements GoodsInnerApi {
    /**
     * 查询商品信息
     *
     * @param goodsId 商品id
     * @return 商品信息
     */
    @Override
    public GoodsVo selectGoods(Integer goodsId) {
        throw new RuntimeException("查询商品信息时熔断");
    }

    /**
     * 扣减库存
     *
     * @param goodsId    商品id
     * @param goodsCount 商品数量
     */
    @Override
    public void deductStock(Integer goodsId, Integer goodsCount) {
        throw new RuntimeException("扣减库存时熔断");
    }
}
