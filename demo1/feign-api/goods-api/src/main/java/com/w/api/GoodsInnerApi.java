package com.w.api;

import com.w.api.hystrix.GoodsInnerApiFailure;
import com.w.vo.GoodsVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @ClassName GoodsInnerApi
 * @Description 商品内部api
 * @Author w²
 * @Date 2020/9/28 0028 14:19
 **/
@FeignClient(name = "goods-server", path = "goods", fallback = GoodsInnerApiFailure.class)
public interface GoodsInnerApi {

    /**
     * 查询商品信息
     *
     * @param goodsId 商品id
     * @return 商品信息
     */
    @GetMapping(value = "selectGoods")
    GoodsVo selectGoods(@RequestParam(value = "goodsId") Integer goodsId);

    /**
     * 扣减库存
     *
     * @param goodsId    商品id
     * @param goodsCount 商品数量
     */
    @GetMapping(value = "deductStock")
    void deductStock(@RequestParam("goodsId") Integer goodsId, @RequestParam("goodsCount") Integer goodsCount);
}
