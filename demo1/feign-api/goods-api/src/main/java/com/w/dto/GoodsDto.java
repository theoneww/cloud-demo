package com.w.dto;

import com.w.base.table.BaseDto;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName GoodsDto
 * @Description 商品数据传输对象
 * @Author w²
 * @Date 2020/9/28 0028 14:32
 **/
@Data
public class GoodsDto extends BaseDto {

    /**
     * 商品名称
     */
    private String name;

    /**
     * 单价
     */
    private BigDecimal price;

    /**
     * 商品数量
     */
    private Integer count;

}
