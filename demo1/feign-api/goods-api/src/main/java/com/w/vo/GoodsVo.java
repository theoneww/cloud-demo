package com.w.vo;

import com.w.base.table.BaseVo;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName GoodsVo
 * @Description 商品视图对象
 * @Author w²
 * @Date 2020/9/28 0028 15:20
 **/
@Data
public class GoodsVo extends BaseVo {

    /**
     * 商品名称
     */
    private String name;

    /**
     * 单价
     */
    private BigDecimal price;

    /**
     * 商品数量
     */
    private Integer count;

}
