package com.w.dto;

import com.w.base.table.BaseDto;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName UserDto
 * @Description 用户数据传输对象
 * @Author w²
 * @Date 2020/9/28 0028 14:59
 **/
@Data
public class UserDto extends BaseDto {

    /**
     * 姓名
     */
    private String name;

    /**
     * 余额
     */
    private BigDecimal balance;

}
