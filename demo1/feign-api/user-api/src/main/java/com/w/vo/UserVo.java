package com.w.vo;

import com.w.base.table.BaseVo;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName UserVo
 * @Description 用户视图对象
 * @Author w²
 * @Date 2020/9/28 0028 15:30
 **/
@Data
public class UserVo extends BaseVo {

    /**
     * 姓名
     */
    private String name;

    /**
     * 余额
     */
    private BigDecimal balance;

}
