package com.w.api;

import com.w.dto.UserDto;
import com.w.api.hystrix.UserInnerApiFailure;
import com.w.vo.UserVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * @ClassName userInnerApi
 * @Description 订单内部api
 * @Author w²
 * @Date 2020/9/28 0028 14:19
 **/
@FeignClient(name = "user-server", path = "user", fallback = UserInnerApiFailure.class)
public interface UserInnerApi {

    /**
     * 查询用户信息
     *
     * @param userId 用户id
     * @return 用户信息
     */
    @GetMapping(value = "selectUser")
    UserVo selectUser(@RequestParam("userId") Integer userId);

    /**
     * 修改用户
     *
     * @param userDto 用户信息
     * @return 修改结果
     */
    @PostMapping(value = "updateUserBalance")
    Boolean updateUserBalance(@RequestBody UserDto userDto);

    /**
     * 扣减余额
     *
     * @param userId     用户id
     * @param orderPrice 订单价格
     */
    @GetMapping(value = "reduceBalance")
    void reduceBalance(@RequestParam("userId") Integer userId, @RequestParam("orderPrice") BigDecimal orderPrice);
}
