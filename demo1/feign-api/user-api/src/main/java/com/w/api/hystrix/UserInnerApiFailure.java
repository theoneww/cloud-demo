package com.w.api.hystrix;

import com.w.api.UserInnerApi;
import com.w.dto.UserDto;
import com.w.vo.UserVo;

import java.math.BigDecimal;

/**
 * @ClassName UserInnerApiFailure
 * @Description
 * @Author w²
 * @Date 2020/9/28 0028 17:11
 **/
public class UserInnerApiFailure implements UserInnerApi {

    /**
     * 查询用户信息
     *
     * @param userId 用户id
     * @return 用户信息
     */
    @Override
    public UserVo selectUser(Integer userId) {
        throw new RuntimeException("查询用户信息时熔断");
    }

    /**
     * 修改用户
     *
     * @param userDto 用户信息
     * @return 修改结果
     */
    @Override
    public Boolean updateUserBalance(UserDto userDto) {
        throw new RuntimeException("修改用户信息时熔断");
    }

    /**
     * 扣减余额
     *
     * @param userId     用户id
     * @param orderPrice 订单价格
     */
    @Override
    public void reduceBalance(Integer userId, BigDecimal orderPrice) {
        throw new RuntimeException("扣减用户余额时熔断");
    }

}
