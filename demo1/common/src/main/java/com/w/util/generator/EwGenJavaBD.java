package com.w.util.generator;


import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author zengliming
 * @date 2018/8/29 13:54
 */
@Data
public class EwGenJavaBD {

    private String name;

    private String mark;

    private List<Map<String,String>> fields;
}
