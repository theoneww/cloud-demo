package com.w.base;


import lombok.Data;

import java.util.List;

/**
 * @author zengliming
 * @date 2018/8/29 16:34
 */
@Data
public final class PageVo<T> {
    /**
     * 数据
     */
    private List<T> data;

    /**
     * 当前页码
     */
    private int pageSize = 1;

    /**
     * 查询多少条数据
     */
    private int pageNum = 10;

    /**
     * 总数
     */
    private long count;

    /**
     * 0
     */
    private int code;

    /**
     * msg
     */
    private String msg;


    public Result toResult(String msg){
        Result result;
        if(code==0){
            result = Result.success(msg==null?this.msg:msg);
        }else{
            result = Result.error(msg==null?this.msg:msg);
        }
        result.setData(this);
        return result;
    }

}
