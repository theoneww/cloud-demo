package com.w.base.table;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName BaseEntity
 * @Description 实体类基础类
 * @Author w²
 * @Date 2020/9/27 0027 14:32
 **/
@Data
public class BaseEntity implements Serializable {

//    /**
//     * 主键id
//     */
//    @TableId(value = BaseTable.ID, type = IdType.ID_WORKER)
//    @TableField(value = BaseTable.ID)
//    private Integer id;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    @TableField(value = "id")
    private Integer id;

}
