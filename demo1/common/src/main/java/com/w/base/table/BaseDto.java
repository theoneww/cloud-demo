package com.w.base.table;

import lombok.Data;

/**
 * @ClassName BaseDto
 * @Description dto基础类
 * @Author w²
 * @Date 2020/9/28 0028 14:39
 **/
@Data
public class BaseDto {

    /**
     * 主键id
     */
    private Integer id;

}
