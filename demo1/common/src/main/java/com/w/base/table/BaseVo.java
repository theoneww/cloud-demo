package com.w.base.table;

import lombok.Data;

/**
 * @ClassName BaseVo
 * @Description Vo基础类
 * @Author w²
 * @Date 2020/9/28 0028 14:39
 **/
@Data
public class BaseVo {

    /**
     * 主键id
     */
    private Integer id;

}
