package com.w.base.table;

/**
 * @ClassName BaseTable
 * @Description 基础表字段映射
 * @Author w²
 * @Date 2020/9/27 0027 14:48
 **/
public class BaseTable {

    /**
     * id字段
     */
    public static final String ID = "id";
    /**
     * 添加时间
     */
    public static final String ADD_TIME ="add_time";
    /**
     * 修改时间
     */
    public static final  String UPDATE_TIME = "update_time";
    /**
     * 版本号字段
     */
    public static final String VERSION = "version";
    /**
     * 是否删除字段
     */
    public static final String DELETE_FLAG = "delete_flag";
    /**
     * 添加人字段
     */
    public static final  String ADD_USER_ID = "add_user_id";
    /**
     * 修改人字段
     */
    public static final  String UPDATE_USER_ID = "update_user_id";

}
