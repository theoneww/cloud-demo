package com.w.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName ConfigController
 * @Description 配置接口类
 * @Author w²
 * @Date 2020/9/22 0022 15:41
 **/
@RestController
@RequestMapping(value = "/config")
@RefreshScope // 实现配置自动更新
public class ConfigController {

    @Autowired
    private TestConfig testConfig;

    @Value("${useLocalCache:false}")
    private boolean useLocalCache;


    @Value("${testRefresh:123}")
    private String testRefresh;

    @RequestMapping("/get")
    public Object get() {
        // 此处如果nacos服务器存在此配置，则返回nacos服务器中配置值。会覆盖本地值。
        System.out.println("testRefresh-->"+testRefresh);
        System.out.println("useLocalCache-->"+testConfig.isUseLocalCache());
        return useLocalCache;
    }

}
