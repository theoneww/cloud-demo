package com.w.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @ClassName ConfigAS
 * @Description 配置测试
 * @Author w²
 * @Date 2020/9/22 0022 16:37
 **/
@Component
@RefreshScope // 实现配置自动更新
public class TestConfig {

    @Value("${useLocalCache:false}")
    private boolean useLocalCache;

    @Value("${testRefresh:123}")
    private String testRefresh;

    public boolean isUseLocalCache() {
        return useLocalCache;
    }

    public void setUseLocalCache(boolean useLocalCache) {
        this.useLocalCache = useLocalCache;
    }

    public String getTestRefresh() {
        return testRefresh;
    }

    public void setTestRefresh(String testRefresh) {
        this.testRefresh = testRefresh;
    }

    public static void main(String[] args) {
        System.out.println(new BCryptPasswordEncoder().encode("nacos"));
    }
}
