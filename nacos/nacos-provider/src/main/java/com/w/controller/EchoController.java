package com.w.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @ClassName EchoController
 * @Description 接口类
 * @Author w²
 * @Date 2020/9/22 0022 17:50
 **/
@RestController
public class EchoController {

    @RequestMapping(value = "/echo/{string}", method = RequestMethod.GET)
    public String echo(@PathVariable String string) {
        System.out.println("我来到provider了..." + new Date());
        return "Hello Nacos Discovery " + string;
    }

}
