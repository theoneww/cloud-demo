package com.w;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName TestController
 * @Description 1
 * @Author w²
 * @Date 2020/9/22 0022 10:16
 **/
@RestController
public class TestController {

    @Value("${test}")
    private String test;

    @GetMapping("/")
    public String re(){
        return test;
    }

}
